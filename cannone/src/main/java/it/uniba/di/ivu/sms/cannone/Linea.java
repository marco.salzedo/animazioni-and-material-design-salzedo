// Line.java
// Class Linea represents a line with two endpoints.
package it.uniba.di.ivu.sms.cannone;

import android.graphics.Point;

public class Linea {
   public Point start; // starting Point
   public Point end; // ending Point
   public Point start2;
   public Point end2;

   // il costruttore di default inizializza Points a (0, 0)
   public Linea() {
      start = new Point(0, 0); // start Point
      end = new Point(0, 0); // end Point
      start2 = new Point(0, 0); // start Point
      end2 = new Point(0, 0); // end Point
   } // end method Linea

   public void setPoint(float offset){
      int x1;
      int x2;
      int y1;
      int y2;
      x1 = start.x + (int) offset;
      y1 = start.y;
      x2 = end.x + (int) offset;
      y2 = end.y;
      start2 = new Point(x1, y1);
      end2 = new Point(x2, y2);
   }
} // end class Linea
